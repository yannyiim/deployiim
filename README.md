# Deployment on GitLab and Heroku

Gitlab repository : https://gitlab.com/yannyiim/deployiim
Github repository : https://github.com/IIM-Creative-Technology/devops-docker-yannyouzid

## Project Url
```
Prod url : https://deployiim.herokuapp.com/
Preprod url : https://deployiim-preprod.herokuapp.com/

```
## Tool needed
```
Postman
```

## Data Class
```
model
availability
price
stock
```

## Api Routes
```
```
### GET
```
https://deployiim.herokuapp.com/shoes  --> List of all data
https://deployiim.herokuapp.com/shoes/_id  --> Get one data with one id

```

### POST
```
https://deployiim.herokuapp.com/shoes  --> Post a new data

```

### PUT
```
https://deployiim.herokuapp.com/shoes  --> Modify the data, you choose the data with id

```

### DELETE
```
https://deployiim.herokuapp.com/shoes  --> Delete the data, you choose the data with id

```

## Stages
```
Stage 1 : Build project
Stage 2 : Tests with jest
Stage 3 : Remove unseless dependencies (jest)
Stage 4 : Deploy
```