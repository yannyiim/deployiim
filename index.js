const express = require("express");
const app = express();
const { MongoClient, ObjectId } = require("mongodb");

const url = "mongodb+srv://admin:admin@cluster0.vfeta.mongodb.net/data?retryWrites=true&w=majority";
const client = new MongoClient(url, { useUnifiedTopology: true });
const dbName = "data";

const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({extended: false}))

let db, shoesDb;

app.get('/', function (req, res) {
  res.send('API Shoes')
})

app.get('/shoes', (req, res) => {
    async function findShoes() {
        const foundShoes = await shoesDb.find().toArray();
        res.json(foundShoes);
    }
    findShoes();
})

app.get('/shoes/:id', (req, res) => {
    async function findOneShoes() {
        const foundOneShoes = await shoesDb.findOne({"_id": ObjectId(req.params.id)})
        res.json(foundOneShoes);
    }
    findOneShoes();
})

app.post('/shoes', (req, res) =>{
    let shoes = new Shoes(req.body.model, req.body.availability, req.body.price, req.body.stock)
    shoesDb.insertOne(shoes)
    res.sendStatus(200)
})

app.delete('/shoes', (req, res) =>{
    shoesDb.deleteOne({"_id": ObjectId(req.body.id)})
    async function findShoes() {
        const foundShoes = await  shoesDb.findOne({"_id": ObjectId(req.body.id)})
        if(foundShoes !== null){
            res.send("The entry was not deleted")
        }
        res.send("The entry was deleted")
    }
    findShoes();
})

app.put('/shoes', (req, res) => {
    console.log(' Shoes router for update ');
    async function findShoes() {
        try{
            const foundShoes = await  shoesDb.findOne({"_id": ObjectId(req.body.id)})
            if(foundShoes !== null){
                let shoes = new Shoes(foundShoes.model, foundShoes.availability, foundShoes.price, foundShoes.stock)
                shoes.model = req.body.model;
                shoes.availability = req.body.availability;
                shoes.price = req.body.price;
                shoes.stock = req.body.stock;

                try{
                    await shoesDb.updateOne(
                        {"_id": ObjectId(req.body.id)},
                        {$set:shoes});
                } catch(err){
                    console.log(err.stack)
                }
                res.send("The shoes were updated");
            } else {
                res.send("The shoes were not updated");
            }}catch(err){
            res.send("Object id is invalid")
        }
    }
    findShoes();
})
 
async function run() {
    try {
        await client.connect();
        db = client.db(dbName);
        shoesDb = db.collection("shoes");
        app.listen(process.env.PORT || 3000);
    } catch (err) {
        console.log(err.stack);
    }
}

run().catch(console.dir);

class Shoes {
    constructor(model, availability = false, price, stock) {
        this.model = model;
        this.availability = availability;
        this.price = price;
        this.stock = stock;
    }
}